# LOGICIEL POUR TESTER UNE API 

___ 

☞ Lorsqu'on développe une api ce qu'il faut faire **c'est la tester**, il faut tester **chaque route**, et tester chaque route avec **les bons paramètre** etc.. On teste les routes avec des **logiciels.** 

☞ On utilise comme logiciel  **Insomnia**, ou **PostMan**. 

_**Post Man** est plus connu. **Insomnia** est plus facile à utiliser_ 


◾️ **Pour crée son espace de travail** 

1. **Télécharger Insomnia**

2. Faire un **create Workspace** et et le **nommer**

3. F**aire une new Request qu'on nomme**

4. Ça va nous afficher ceci (voir img)

![img_1.png](../assets/img-api/insomnia.png)

***Ça va afficher deux élément, l'url qu'il faut que je requete et la méthode***

☞ Lire une documentation d'API 

![img_1.png](../assets/img-api/api4.png)

- `curl` : Indique qu'on peut faire une requête http sur cette url 

- `https://api-adresse.data.gouv.fr`: Est le nom de domaine 

- `/search/?q=8+bd+du+port` : La route 
   - `q=8+bd+du+port` : Query Params 

___

⏯ - API - Logiciel pour tester une api => https://youtu.be/tTbCkY0oIaA