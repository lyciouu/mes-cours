# API 

___

- `Node JS` : **Moteur d'éxécution** Java Script basé sur le **moteur** **V8** Java Script de **Google Chrome** qui nous permet d'**intérpêter** du du Java Script **côté serveur** 



- `API` : **Accronyme**  **A**pplication **P**rogramming **I**nterface, c'est donc une **interface de programmation applicative**,**** l'idée dérriere ça c'est de **fournir une interface** entre **deux programmes**. Une interface peut être constitués de 
   - **_class_**
    - **_méthodes_** 
    - **_modèle_** 
    - **_fonction_**
    - **_constante_**
    
qui va etre **fourni à un autre programme**. 

**Pour faire simple : c’est un moyen de communication entre deux logiciels, que ce soit entre différents composants d’une application ou entre deux applications différentes.**

![img_1.png](../assets/img-api/api.png)
    

    
`️UNE API C'EST UNE INTERFACE QUI PERMET À DEUW PROGRAMMES DE COMMUNIQUER ENTRE EUX`

![img_1.png](../assets/img-api/api5.png)

> Les API permettent la communication entre de nombreux composants différents de votre application, mais aussi entre des composants de votre application et d’autres développeurs. Elles agissent ainsi comme un intermédiaire qui transmet des messages à travers un système de requêtes et de réponses.

 
◾ **️Il y a 3 étapes :** 

- Le consomateur **demande** une donnée au **producteur** 
- Le producteur **crée la réponse** 
- Et **l'API va retourner la réponse** 

***La requete est constituer d'une requete et d'une réponse.***  

![img_1.png](../assets/img-api/api2.png)

![img_1.png](../assets/img-api/api3.png)

___

⏯ - API - Présentation => https://youtu.be/qPSx0CHKbUM