# GÉRER LES ERREURS ET CHANGER LE STATUS DE LA RÉPONSE 

___

⤵️ Exemple : 
```js

const express = require('express');

const server = express();


server.use(express.json()); //Middleware /parser le JSON donc les payload/body


// GET /
server.get('/', (request,response) => {
    response.send('<h1>hello world</h1>');
});

// GET /users
server.get('/users', (request,response) => {
    const users = [];
    response.send({users});
});

// POST /users
server.post('/users', (request,response) => {
    console.log(request.body);
    response.statusCode = 404; // Renvoie un status
    response.send({message : 'User saved', user: 'Lycia', req: request.body.user});
});

// PUT /users/:id
server.put('/users/:id', (request,response) => {
    response.send({message: 'User updated successfully', user: request.params.id, data: request.body})
});

// DELETE /users/:id
server.delete('/users/:id', (request,response) => {
    console.log(request.params);
// vérifie que l'id corresponds à un utilisateur dans la base de données
    const userExist=false;
    if(userExist===false){
        response.status(404).send('User not found : '+ request.params.id)
    }
    response.status(201).send({message: 'User successfully deleted', user: request.params.id})
// Renvoie le status 
});

server.listen(8080, () => {
    console.log('Server running on http://localhost:8080');
});

```

⏯ -  API - Gérer les erreurs et changer le status de la réponse => https://youtu.be/8tr82LyH46Q