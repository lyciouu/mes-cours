# PRÉSENTATION ET INSTALLATION DE EXPRESSJS 

___

️ ☞ Express est une **infrastructure** **Web minimaliste**, **souple** et **rapide** pour **Node.js** car il fournit une **couche fine de fonctionnalitées** qui nous permette de **développer des infrastructures web en NodeJs** tout en gardant les **modules NodeJs**, le **http** etc .. 

◾️ **Installation ExpressJs** 

1. Crée un dossier 
2. Ce rendre dans le dossier avec la commande **cd** suivi du **nom du dossier** 
3. Écrire la commande **npm init** 
4. Cela va crée un fichier un **package.json**

![img_1.png](../assets/img-api/express.png)

5. Écrire la commande **npm install express --save**
6. Cela va crée un dossier **node_modules** qui contiendra **toutes les dépendance du projet** 
![img_1.png](../assets/img-api/express2.png)
   
7. Crée un fichier **.gitignore** et ignorer le fichier **/node_modules* et **/.idea** 

___

⏯ -  API - Présentation et installation de ExpressJs => https://youtu.be/Mupb9Ku7r3w

➡️ - https://expressjs.com/fr/starter/installing.html