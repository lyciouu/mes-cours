# SOMMAIRE COURS 
___

* ## POINT LIVE AVEC CHALOM 

01. [Cour du 18.02.2021](./00.point-live/01.cour-chlalom-1802.md)
02. [Cour du 24.02.2021](./00.point-live/02.cour-chalom-2402.md)
03. [Cour du 26.02.2021](/00.point-live/03.cour-chalom-2602.md)


* ## GITLAB - MARKDOWN

01. [Gitlab-Markdown](./01.gitlab-mardown/01.gitlab-markdown.md)

___

* ## HTML CSS

01. [Cour sur le html](./02.html-css/01.html.md)
02. [Cour sur le css](./02.html-css/02.css.md)
03. [Flebox](./02.html-css/03.flexbox.md)
04. [Bootsrap](./02.html-css/04.bootstrap.md)

___

* ## JAVASCRIPT 

01. [Introduction](./03.javascript%20/01.intro-javascript.md)
02. [Opérateur](./03.javascript%20/02.operateur-javascript.md)
03. [Strings : Chaine de caractère](./03.javascript%20/03.strings-javascript.md)
04. [Array : Tableau](./03.javascript%20/04.array-javascript.md)
05. [Conditions](./03.javascript%20/05.conditions-javascript.md)
06. [Boucles](./03.javascript%20/06.boucles-javascript.md)
07. [Fonctions](./03.javascript%20/07.fonction-javascript.md)
08. [Objet](./03.javascript%20/08.objet-javascript.md)
09. [Spread Operator](./03.javascript%20/09.spread-operator-javascript.md)
10. [Affecter par Décomposition](./03.javascript%20/10.affecter-par-decomposition-javascript.md) 
11. [Manipuler les Tableaux](./03.javascript%20/11.manipuler-tableau.javascript.md)
12. [Les Classes](./03.javascript%20/12.classe-javascript.md)
13. [Manipuler le Dom](./03.javascript%20/13.manipuler-le-dom.javascript.md)
14. [Fonction Arrow](./03.javascript%20/14.fonction-arrow-javascript.md)
___

* ## TYPE SCRIPT 

01. [Présentation Node. JS](./04.typescript/01.presentation-nodejs-typescript.md)
02. [Présentation Npm](./04.typescript/02.presentation-npm-typescript.md)
03. [Présentation Type Script](./04.typescript/03.presentation-typescript-typescript.md)
04. [Package Node. JS](./04.typescript/04.package-nodejs-typescript.md)
05. [Basic Types](./04.typescript/05.basic-type-typescript.md)
06. [Functions](./04.typescript/06.functions-typescript.md)
07. [Enums](./04.typescript/07.enums-typescript.md)
08. [Tuples](./04.typescript/08.tuples-typescript.md)
09. [Literal Types](./04.typescript/09.literal-types-typescript.md)
10. [Types Assertions](./04.typescript/10.types-assertions-typescript.md)
11. [Interfaces](./04.typescript/11.interface-typescript.md)
12. [Class](./04.typescript/12.class-typescript.md)
13. [Parameter Properties](./04.typescript/13.parameter-properties-typescript.md)
14. [Import Export Modules](./04.typescript/14.import-export-modules-typescript.md)
15. [Ts Config](./04.typescript/15.ts-config-typescript.md)

___

* ## ANGULAR 

00. [Fonction Arrow | This | New](./05.angular/00.arrow-this-new-angular.md)
01. [Culture D'Angular](./05.angular/01.culture-angular.md)
02. [Setup Angular](./05.angular/02.setup-angular.md)
03. [Présentation de la Structure](./05.angular/03.presentation-structure-angular.md)
04. [Installer Bootstrap](./05.angular/04.install-bootstrap-angular.md)
05. [Premier Composant](./05.angular/05.premier-composant-angular.md)
06. [Interpolation Property Binding](./05.angular/06.interpolation-property-binding-angular.md)
07. [Event Biding](./05.angular/07.event-biding-angular.md)
08. [Two Way Data Binding](./05.angular/08.two-way-data-binding-angular.md)
09. [Structural Directive ngIf](./05.angular/09.structural-directive-ngif-angular.md)
10. [Structural Directive ngFor](./05.angular/10.structural-directive-ngfor-angular.md)
11. [Input Decorator](./05.angular/11.input-decorator.md)
12. [Emettre des données au composant Parent](./05.angular/12.emettre-des-donnée-au-composant-parent-angular.md)
13. [Modules](./05.angular/13.modules-angular.md)
14. [Routing](./05.angular/14.routing-angular.md)
15. [Routing Params](./05.angular/15.routing-params-angular.md)
16. [Routing Router Service](./05.angular/16.routing-router-service-angular.md) 
17. [Component LifeCycle](./05.angular/17.component-lifecycle-angular.md)
18. [Programmation réactive librairie RxJs](./05.angular/18.programmation-reactive-librairie-rxjs-angular.md)
19. [Service Injection](./05.angular/19.service-injection-dépendances-angular.md./)
___

* ### REACT 

01. [Présentation React](./06.react/01.presentation-react.md)
02. [Premier Élémént ](./06.react/02.premier-element-react.md)
03. [Syntaxe Jsx](./06.react/03.syntaxe-jsx-react.md)
04. [Déclarer un composant](./06.react/04.declarer-composant-react.md)
05. [Component LifeCycle](./06.react/05.component-lifecycle-react.md)
06. [Component State](./06.react/06.component-state-react.md)
07. [Gérer Évenements](./06.react/07.gerer-evenement-react.md)
08. [Affichage Conditionel](./06.react/08.affichage-conditionel-react.md)
09. [Liste Key](./06.react/09.liste-key-react.md)
10. [Faire remontée la donnée ](./06.react/10.faire-remonter-la-donnée-react.md)
11. [Creat React App](./06.react/11.create-react-app-react.md)
12. [Formulaire](./06.react/12.formulaire-react.md)
13. [Router](./06.react/13.router-react.md)
14. [Axios http Client](./06.react/14.axios-http-client-react.md)
___

* ### API 

01. [Présentation API](./07.api/01.presentation-api.md)
02. [HTTP](./07.api/02.http-api.md)
03. [Logiciel Test API](./07.api/03.logiciel-test-api-api.md)
04. [Première API NodeJs](./07.api/04.premeire-api-nodejs-api.md)
05. [Presentation et Installation ExpressJs](./07.api/05.presentation-installation-expressjs-api.md)
06. [Crée un Serveur Htpp avec ExpressJs](./07.api/06.cree-server-http-expressjs-api.md)
07. [Paramètre de route et contenue de la requête ](./07.api/07.parametre-route-contenue-requete-api.md)
08. [Gérer les erreurs et changer le status de la réponse](./07.api/08.gerer-erreurs-api.md)

___

* ### CLEAN ARCHITECTURE 

01. [Clean Architecture](./08.clean-architecture/01.clean-architecture.md)

___

* ### MySQL

01. [Définition MySQL](./09.mysql/01.def-mysql.md)
02. [Installation de MySQL](./09.mysql/02.intallation-mysql.md)
03. [Connexions au Serveur MySQL](./09.mysql/03.connexion-server-mysql-mysql.md)
04. [Crée une Table et Découverte d'Options](./09.mysql/04.cree-une-table-decouverte-options-mysql.md)
05. [Configuration Tables Produit](./09.mysql/05.config-table-produit-mysql.md)
06. [Insérer des données - INSERT INTO](./09.mysql/06.inserer-des-données-insert-into-mysql.md)
07. [Supprimer des données - DELETE FROM](./09.mysql/07.supprimer-des-données-delete-from-mysql.md)
08. [Mise à jour des données - UPDATE](./09.mysql/08.mise-à-jour-des-données-update-mysql.md)
09. [Sélectionner des données - SELECT](./09.mysql/09.selectionner-donneés-select-mysql.md)
10. [Trier les donner - ORDER BY](./09.mysql/10.trier-les-données-order-by-mysql.md)
11. [Limiter les données séléctionnées - LIMIT](./09.mysql/11.limiter-donnée-sélectionnées-limit-mysql.md)
12. [Compter les données - COUNT](./09.mysql/12.compter-les-données-fonction-count-mysql.md)
13. [Grouper les données par Groupe](./09.mysql/13.grouper-les-données-compter-par-groupe-mysql.md)
14. [Additionner les données - SUM](./09.mysql/14.additionner-les-données-sum-mysql.md)
15. [Rechercher des données de manière Partielle](./09.mysql/15.rechercher-des-données-de-manière-partielle-mysql.md)

___

* ### SEQUELIZE

01. [ORM](./10.sequelize/01.orm.sequelize.md)
02. [Sequelize](./10.sequelize/02.sequelize-sequelize.md)
03. [Installation](./10.sequelize/03.intallation-sequelize.md)
04. [First Connection](./10.sequelize/04.first-connection-sequelize.md)
05. [Défénir le modèle user](./10.sequelize/05.definir-le-model-user-sequelize.md)
06. [Synchronisation des modèles](./10.sequelize/06.synchronisation-des-modeles-sequelize.md)
07. [Enregistrer un Utilisateur](./10.sequelize/07.enregistrer-un-utilisateur-sequelize.md)
08. [Récupéré la listes des Utilisateurs](./10.sequelize/08.récupéré-la-listes-utilisateurs-sequelize.md)
09. [Trouver un Utilisateur grâce à sa clé Primmaire](./10.sequelize/09.touver-un-utilisateur-grace-à-sa-clé-primmaire-sequelize.md)

___

* ### MONGODB

01. [Introduction](./11.mongodb/01.introduction-mongodb.md)
02. [Présentation MongoDB](./11.mongodb/02.presentation-mongodb-mongoose-mongodb.md)
03. [Installation de MongoDB](./11.mongodb/03.installation-mongodb-mongodb.md)
04. [Préparation d'une nouvelle API](./11.mongodb/04.préparation-d'une-nouvelle-api-mongodb.md)
05. [Créer une connexion](./11.mongodb/05.creer-une-connexion-mongodb.md)
06. [Télécharger Compass pour Visualiser les données](./11.mongodb/06.%20télécharger-compass-pour-visualiser-les-données-mongodb.md)
07. [Définir le model](./11.mongodb/07.defenir-modele-mongodb.md)
08. [Créer une Company](./11.mongodb/08.créer-une-company-mongodb.md)
09. [Récupérer la liste des Compagnies](./11.mongodb/09.récupérer-la-liste-des-compagnies-mongodb.md)
10. [Afficher les informations d'une Compagnie](./11.mongodb/10.afficher-les-informations-d'une-compagnie.md)
11. [Mise à jour d'une Compagnie](./11.mongodb/11.%20mise-à-jour-d'une-compagnie-mongodb.md)
12. [Refactoring Router](./11.mongodb/12.refactoring-router-mongodb.md)
13. [Refactoring Controller](./11.mongodb/13.refactoring-controller-mongodb.md)
14. [Refactoring la connexion à la base de données](./11.mongodb/14.refactoring-la-connexion-à-la-base-de-données-mongodb.md)