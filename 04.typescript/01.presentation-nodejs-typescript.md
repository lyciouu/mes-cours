# PRESENTATION NODE JS
___

☞ __Node. JS__ est un moteur __d'éxécution__ du language Javascript.
__Node.js__ est très couramment utilisé pour écrire des services côté serveur appelés API (interfaces de programmation d’application).

◾️ __Instalation NODE JS__ 

- Aller sur : https://nodejs.org/en/
- Télécharger la dernière version

◾️ __Vérifier que Node. JS est bien installé__

- Ouvrir le terminal 
- Écrire : node --version 
- Appuyer sur entrée, la version télécharger sera affiché 


☞ Avec __Node. JS__ on peut par exemple crée une serveur avec HTTP, HTTPS etc.. 

`ATTENTION:  LE CODE QUI EST DÉVELOPPEZ POUR LE SERVEUR EST EXCUCTÉ QUE PAR LE SERVEUR ET NON PAR LE NAVIGATEUR  `

___

⏯ - Typescript - presentation nodejs => https://youtu.be/8Ak786GELow

➡️ - https://nodejs.org/en/

➡️ - https://frenchco.de/node-js-cest-quoi/