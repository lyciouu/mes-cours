# TYPES ASSERTIONS 
___



`````typescript
let input: string = '25' ; // PAGE HTML 
let var1:  number; 

var1 = input; // ❌ ERREUR string et number n'ont pas la meme valeur ❌

var1 = input as any ; // ✅ Any veut dire qu'on la consière comme n'importe quelle donnée ✅

// AUTRE SYNTAXE 

var1 = <any>input; // 

`````
___
 ⏯ - Typescript - types assertions => https://youtu.be/Osl7mcX0jbs