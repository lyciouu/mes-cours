# MANIPULER LE DOM

___

### SOMMAIRE 

1. [Manipuler le dom : S'assurer que le dom est chargé](#sassurer-que-le-dom-est-charge)
2. [Manipuler le dom : Sélectionner un élément grâce à son id](#selectionner-un-element-grace-a-son-id)
3. [Manipuler le dom : Sélectionner des éléments grâce à leurs classes](#selectionner-des-elements-grace-a-leurs-classes)
4. [Manipuler le dom : Sélectionner un ou des éléments](#selectionner-un-ou-des-elements)
5. [Manipuler le dom : Manipuler le style d'un élément](#manipuler-le-style-d-un-element)
6. [ Manipuler le dom : Manipuler les classes d'un élément](#manipuler-les-class-d-un-element)
7. [Manipuler le dom : Créer et ajouter au dom un élément](#creer-et-ajouter-au-dom-un-element)
8. [Manipuler le dom : Écouter les évènements du dom ](#ecouter-les-evenements-du-dom)
9. [Lien important](#lien-important)

___

### S'ASSURER QUE LE DOM EST CHARGE

☞ Le __DOM__,  signifie Document Object Model (c'est-à-dire "modèle d'objet de document", en français), est une __interface de programmation__ qui est une représentation du HTML d'une page web et qui permet __d'accéder aux éléments__ de cette page web et de les __modifier avec le langage JavaScript__.

Lorsque le navigateur récupère la page __HTML__ il va éxécuté chaque ligne les unes après les autres. Il va d'abord interpréter tout les les éléments qui se trouve dans la balise `<head></head>` puis ensuite il va s'occupeer d'interprété la structure de la page internet. 
 
Les éléments en __javascript__ on ne les lie pas à la page html dans la balise `<head></head>` mais à la fin dans la balise `<body></body>`, car si on a des scripts très complexe ( à interpréter ou à télécharger ), cela va ralentir l'interprétation de la structure de la page. 

☞ Pour liée le un fichier javascript, à une page html : 

__VOIR COUR__ : [INTRODUCTION : JAVASCRIPT](01.intro-javascript.md)

☞ Pour s'assurer que le DOM est chargé il y a deux méthodes :

```javascript

           // PREMIÈRE METHODE

window.onload = function() { // fonction callback 
  console.log('4');
  console.log(document.querySelector('h1')); // on récupère le titre de la page html 
} // cela affichera le résultat en dernier car il va attendre que la page finisse sont chargement 

          // SECONDE MÉTHODE
          
window.addEventListener('DOMContentLoader',function() { //fonction callback
  console.log('5');
  console.log(document.querySelector('h1')); // récupère le titre de la page html
}) // cela affichera le résultat en dernier car il va attendre que la page finisse sont chargement 

```

⏯ - Javascript - Manipuler le dom : S'assurer que le dom est chargé : https://youtu.be/EePywdhU1fk

___

### SELECTIONNER UN ELEMENT GRACE A SON ID 

☞ __ID__ est __unique__ à chaque élément. 

Dans l'objet __document__ on a accés à des prorpiétés ainsi que des méthodes qui vont nous permettre de manipuler des documents. 

`ATTENTION : L'OBJET DOCUMENT NE PREND PAS DE 'S' `

> Il est préférable d'utiliser les __ID__ pour le javascript pour manipuler les éléments avec le js, et non pour styliser les éléments. Pour styliser les éléments on va favoriser l'utilisation des __class__. 


````javascript
const square = document.getElementById('sqr'); // on veut récupéré un élément grace à son id qui est 'sqr'. On la stock dans une constante 
console.log(square); // on affiche l'élément récupéré dans la console, qu'on va pouvoir manipuler
````

⏯ - Javascript - Manipuler le dom : Sélectionner un élément grâce à son id : https://youtu.be/H1OO9ssjVqs

___

### SELECTIONNER DES ELEMENTS GRACE A LEURS CLASSES 

☞ En html une class peut être appliqué que plusiseurs, donc on recevrra toujours en js  une sorte de tableau html

````javascript
  const squares = document.getElementByClassName('square');
  console.log(squares);


````
⏯ -  Javascript - Manipuler le dom : Sélectionner des éléments grâce à leurs classes : https://youtu.be/esFTGa7WLIU

___

### SELECTIONNER UN OU DES ELEMENTS 

☞ Pour séléctionner un élément il faut  utiliser la méthode __querySelector()__ 

- Pour un ID : __#__
  
- Pour une class : __.__

- Pour une balise ou une section : __rien__

 Pour séléctionner grace à une class :

````javascript
// on stock l'élément dans une constante
const squares = document.querySelector('.square') ; // pour séléctionner grace à une class mettre un point
console.log(squares); // retourne le premier élément 
````

⤵️ ️Pour séléctionner un éléments grâce à plusieurs class : 

`````javascript
const square = document.querySelector('.square.bigSquare') ; // pour séléctionner des élément grace à plusieurs class on rajoute encore un point et on nomme la seconde class
console.log(square); // retourne le premier élément qui as les deux class 
`````

⤵️ Pour séléctionner un élément grâce à son id :

````javascript
const square = document.querySelector('#sqr') ;// mettre un #
console.log(square); // retourne id 
````

⤵️ Pour séléctionner des élément grâce au nom de la balise :

`````javascript
const square = document.querySelector('section') ;// mettre le nom de la balise
console.log(square); // retourne la balise
`````

☞ Pour pour séléctionner plusieurs éléments utiliser la méthode __querySelectorAll()__

⤵️ Pour séléctionner plusieurs class :

`````javascript
const squares = document.querySelectorAll('.square') ;// mettre un #
console.log(squares); // retourne les class square
`````

⏯ - Javascript - Manipuler le dom : Sélectionner un ou des éléments : https://youtu.be/MYdFSsFooeM

___

### MANIPULER LE STYLE D UN ELEMENT 


☞ Pour manipuler un style d'élément il faut d'abord récupéré l'élément, et ensuite on modifie le style en utilsans la propriété __style__ 

````javascript
const square = document.querySelector('.square'); // l'élément 

square.style.backgroundColor = 'yellow'; // on modifie la couleur de l'élément séléctionner
square.style.border = '1px solid grey' ; // on ajoute une bordure à l'élément 

square.style.position = 'fixed';
square.style.top = '0px'; 

// modification de manière dynamique qui va faire bouger l'élément en diagonnal

let positionY = 0 ;
setInterval(function () { // prend en premier argument une fonction callback
    positionY++ // on incrémente 
    square.style.top = positionY + 'px';  // on déplace l'élément avec la valeur de la position
}, 100) // toute les seconde on execute la fonction l'élément bouge

let positionX = 0;

setInterval(function () {
    positionX++
    square.style.left = positionX +'px'; // déplace l'élement par rapport a la gauche 
    
}, 100)
console.log(square )
````

☞ Pour changer de couleur de manière dynamique avec le js 

````javascript
const square = document.querySelector('.square'); // l'élément 

square.style.backgroundColor = 'yellow'; // on modifie la couleur de l'élément séléctionner
square.style.border = '1px solid grey' ; // on ajoute une bordure à l'élément 

let i = 0; 
setInterval(function () { // fonction callback
    square.style.backgroundColor = i % 2 ? 'red': 'yellow'; //on le fait changer de couleur toute les seconde
    i++ ; // incrémente
    
}, 1000);

console.log(square) // sa changera de couleur le carrer toute les seconde en rouge puis jaune

````
⏯ - Javascript - Manipuler le dom : Manipuler le style d'un élément : https://youtu.be/H_Lq125g42I

___

### MANIPULER LES CLASS D UN ELEMENT 

☞ Pour manipuler les class d'un élément on doit d'abord récupéré l'élément 

````javascript
const square = document.querySelector('square') ;
console.log(square);

let i = 0;
setInterval(function () { // fonction callback 
    if(i % 2) { // si i est pair 
        square.classList.add('bigSquare') // pour ajouter une class on doit d'abord accéder a la propriété 'classList' puis on accède à la méthode 'add()' et entre () on donne la class que l'on veut ajouter 
    } else { // sinon 
        square.classList.remove('bigSquare') // on retire bigsquare
    }
    
    i ++ ; // incrémente
}, 1000) //second argument 
````
⏯ - Javascript - Manipuler le dom : Manipuler les classes d'un élément : https://youtu.be/b5Th1rw_73Y

___

### CREER ET AJOUTER AU DOM UN ELEMENT 

☞ Pour crée un élément on appelle la méthode __createElement()__ dans l'objet global __element__

````javascript
window.addEventListener('DOMContentLoaded', function() {  
    console.log('DOM CHARGÉ !');
    
  const square =  document.createElement('div');
  square.classList.add('square'); // on rajoute la class square à l'élément
 // square.innerText = 'ok'; // on rajoute du text à l'élément
   // square.innerHTML = <strong> ok </strong> // on rajoute du html dans un élément 
    document.body.appendChild(suqare) // ajouter l'élément dans notre page
    
    console.log(square);
})
````

⏯ - Javascript - Manipuler le dom : Créer et ajouter au dom un élément : https://youtu.be/BQjKZZ2QtpQ
___

### ECOUTER LES EVENEMENTS DU DOM 

☞ Les évènements sont des __actions__ qui se produisent et auxquelles on va __pouvoir répondre en exécutant un code__. Par exemple, on va pouvoir afficher ou cacher du texte suite à un clic d’un utilisateur sur un élément, on change la taille d’un texte lors du passage de la souris d’un utilisateur sur un élément.
__Les évènements et leur prise en charge sont l’un des mécanismes principaux du JavaScript qui vont nous permettre d’ajouter un vrai dynamisme à nos pages Web.__

⤵️ Exemple :
`````javascript
const btnEl = document.getElementById('createSquare');
console.log(btnEl);

const redSquareContainerEl = document.getElementById('redSquare');

btnEl.addEventListener('click', function () { // pour ajouter une écoute d'événement on appel le bouton, en premier argument on donne le type de l'événement qu'on veut écouter, en deuxieme argument fonction callback
     console.log('click', this ); //this fais référence à l'élement sur lequel l'événement  a été appliqué 
    // on peut manipuler l'événement grâce au this 
     
    // pour crée et ajouter un carrer 
    
    const square = createSquare ();
    console.log(square);
    
    redSquareContainerEl.appendChild(createSquare())
}); 

function createSquare (){
    const el = document.createElement('div');
    el.classList.add('square');
    return el;
}

`````


⏯ - Javascript - Manipuler le dom : Écouter les évènements du dom : https://youtu.be/X3HvOQx-hBQ

___
### LIEN IMPORTANT 

➡️ https://www.damienflandrin.fr/blog/post/comprendre-et-manipuler-le-dom-en-javascript

➡️ https://developer.mozilla.org/fr/docs/Web/API/Document_Object_Model