# LES CONDITIONS EN JAVASCRIPT
___

### SOMMAIRE

1. [Les Conditons : If..Else](#les-conditions-if-else)
2. [Les Conditions :Le Ternaire](#le-ternaire)
3. [Les conditions : Switch](#conditions-switch)

___

### LES CONDITIONS IF ELSE

☞ Les conditions sont très utilisé lorsqu'on développe des scripts, des sites webs ou des application pour réagir au différente valeur de notre application. 

⤵️ Exemple :
`````javascript
const age = 25;
if (age >=18) { //il faut avoir entre les () une expresssion, une condition qui doit être vrai
    console.log("Vous êtes majeur");
}
else if (age >= 13 && age <=18){ // else if est une contre condition 
    console.log("Vous êtes un ado");
}
else {
    console.log ("Vous êtes mineur");
}
`````
`ATTENTION : TOUJOURS OUVRIR LES {} après la condition`

_Il n'est pas obligatoire d'afficher un else if_

-> Dans un bloc d'instructions on peut aussi rajouter des nouvelles conditions 

⤵️ Exemple : 

`````javascript
let age = 25 ;
let nation = 'France';
if (age >=18) {
    
    if(nation === 'France') {
        console.log('Vous êtes majeur');
    } else if (nation === 'UK') {
        console.log('Vous êtes jeune')
    }
    else {
        console.log('Vous êtes mineur')
    }
}

`````

__QUAND UNE PREMIÈRE EXÉCUTION EST EXÉCUTÉ, LES AUTRES NE SERONT PAS PRISES EN COMPTE__

⏯ - Javascript - Les conditions : if... else : https://youtu.be/7UxNwWb9wYQ
___

### LE TERNAIRE

☞ Le ternaire est un opérateur conditionnel, de plus c'est un opérateur qui à trois opérandes.

Le ternaire ce présente comme ceci : 

__CONDITION ? siExVrai : siExFaux__

`````javascript
// Si la variable "présence" est true
// Alors j'affiche "Bonjour" dans la console
// Sinon j'affiche "Bye"

let presence = true;
if (presence === true){
    console.log("Bonjour");
} else { console.log("Bye");
}
// AVEC LA MÉTHODE TERNAIRE
let presence = true ;
console.log(presence === true ? "Bonjour" : "Bye"); 
`````
⏯ - Javascript - Les conditions : Le ternaire : https://youtu.be/IvnGha-01mI
___

### CONDITIONS SWITCH 

☞ La conditions switch permet de réaliser des conditions en fonction de l'expression 

⤵️ Exemple :

`````javascript
let fruits = 'Oranges';
if (fruits === 'Pommes'){
    console.log('les pommes sont à 1€ le kilo');
} else if (fruits === 'Fraises') {
    console.log('les fraises sont à 2€ le kilo');
} else if (fruits === 'Bananes') {
    console.log('les bananes sont à 3€ le kilo');
} else {
    console.log('vous n\'avez pas de fruit');
}

// A LA PLACE D'ECRIRE PLUSIEURS SOUS CONDITIONS ON PEUT UTILISER LE SWITCH CASE

let fruits = 'Oranges';
switch (fruits) {
    case 'Pommes':
        console.log('les pommes sont à 1€ le kilo');
        break
    case 'Fraises':
    case 'Fraise de France':   // possibilité de mettre deux case si la conclusion est la meme  
        console.log('les fraises sont à 2€ le kilo');
        break
    case 'Bananes':
        console.log('les bananes sont à 3€ le kilo');
        break  
    default:
        console.log('vous \'avez pas de fruit');
}
````` 
☞ Pour écrire en switch il faut mettre entre () l'expression que je veux tester dans le cas de l'exemple c'est 'Fruits'

__CASE : LE CAS__

__BREAK : CASSER LE SWITCH__

`ATTENTION IL FAUT BIEN ALIGNER LES 'CASE' AFIN D'AVOIR UNE BONNE INDENTATION`

☞ Pour avoir 2 cas qui ont la même réaction mettre 2 cas à la suite (Voir exemple)

☞ Il est possible d'ajouter un mot clé __default:__ pour mettre le comportement par défaut. Le défaut est optionnel. 

`ATTENTION : SI A CHAQUE FOIS ON VEUT DÉCLARER UNE VARIABLE, IL FAUT APRES LES ':' OUVRIR UNE INSTRUCTION { ET FERMER APRÈS LE BREAK }`

⏯ - Javascript - Les conditions : Switch => https://youtu.be/2qDKA7RYWiE