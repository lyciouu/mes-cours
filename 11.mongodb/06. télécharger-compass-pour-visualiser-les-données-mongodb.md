# TÉLÉCHARGER COMPASS POUR VISUALISER LES DONNÉES 
___


◾️ Pour visualiser des données

1. Ce rendre sur le site : **mongodb.com/products/compass**
2. Cliquer sur **Try it now** 
3. Puis **télécharger le logiciel**
4. Ecrire l'url :  **mongodb://127.0.0.1/mean**

![img_1.png](../assets/img-mongodb/compass.png)


![img_1.png](../assets/img-mongodb/compass2.png)

___

⏯ - MongoDB - télécharger compass pour visualiser les données => https://youtu.be/2hFbTj9BHJc