# PRÉPARATION D'UNE NOUVELLE API
___


⤵️ Exemple :

- **Faire un un npm init pour installer un package**
- **Installer express**
- **Installer cors** 




````js
// fichier server.js 

const express = require ('express'); // on récupère express
const cors = require ('cors'); // on récupère cors

const server = express(); // crée un serveur en exuctant la fonction express
const PORT = 8080; // crée une constant PORT


server.use(cors ({
    origin: '*',
    optionsSuccessStatus: 200,
}));


// CRUD COMPANY

// L => LISTE

server.get ('/companies', (req, res) => {
    const companies = ['WR']; // liste de compagnies
    res.send(companies); // réponse
});

// C => CREATE

server.get('/companies/create', (req, res) => {
    res.send({categories: []}); // on renvoies des données qui seront nécessaire pour la company

});
// SAUVEGARDE OU L'INSERTION EN BDD

server.post('/companies', (req, res) => {
    res.send({message: 'Company created successfully'}); // on envoie une réponse avec un msg
});


// R => READ => SHOW

server.get('/companies/:id', (req, res) => {
    const company = {};
    res.send(company); // on renvoie les info de la company
});


// U => UPDATDE

server.get('/companies/edit/:id', (req, res) => {
    const id = req.params.id;
    const company = {};

    res.send({company, categories: []});
});

// SAUVEGARDER OU METTRE À JOUR LA COMPAGNIE

server.put('/companies/:id',(req,res) => {
    res.send({message: 'Company updated successfully'});
});

// D => DELETE

server.delete('/companies/:id', (req, res) => {

    res.send({message: 'Company deleted successfully'})
});


server.listen(PORT, '127.0.0.1', function(){  // ecoute sur 8080 et sur 127.0.0.1 et on execute la fonction
    console.log('Server running on http://localhost:' + PORT);
});
````

___

⏯ -  MongoDB - préparation d'une nouvelle api => https://youtu.be/YsSabmoHH2U