# INTRODUCTION 

___

☞ **mongoDB** est un **système de gestion de base de données orienté document**. 
Le package **Mangoose** qui nous permet de **connecter une application Node JS** avec **MongoDB**.****
Avec cette formation nous complétons ce qu'on appelle la **mean stack**

- `mean` : **M**ongoDB **E**xpressJS **A**ngular **N**odeJS 

avec ses **4 technologies** totalement basé sur **JavaScript** il est possible de **couvrir la totalité des composantes nécessaires** à la **création de sites web dynamiques** ou d'**application web**. 
Il est possible de développer un **projet complet en JavaScript sur 3 niveaux** :

- `Le niveau d'affichage`:  avec **Angular** ou **React** 
- `Le niveau d'application` : avec **Express** et **Node JS** 
- `Le niveau de base de données`:  avec **mongoDB** 

Un développeur **fullstack** maîtrise un minumum les technologies liées à ces **3 niveaux** qui amène à la conception et à la réalisation de **projets web complet**. 

___ 

⏯ - MongoDB - introduction => https://youtu.be/zqwAOjVwCFM

