# REFACTORING ROUTER 

___

☞ La refactorisation c'est la retouche du code sans y apporter de nouvelles fonctionnalitées. 

⤵️ Exemple : 

- **Crée un dossier Router** 
- **Crée un fichier CompaniesRouter.js**

````js
// fichier CompaniesRouter

const express = require ('express');

const router = express.Router();
const Company = require ('../models/Company')
const prefix = '/companies';
// CRUD COMPANY

// L => LISTE

router.get('/', (req,res) =>{
    const query = Company.find();
    query.select('id name description');
    query.exec((err,companies) => {
        res.send(companies)
    });

});

/*
// récupéré toutes les companies
server.get ('/companies', (req, res) => {
     Company.find((err,companies) =>{
    res.send(companies);
    });
}); */

/*
// récupéré une company grace à son ID
  server.get ('/companies', (req, res) => {
     Company.findById( "60507006cb63620b09b4c2ed",(err,company) =>{
    res.send(company);
    });
}); */

// C => CREATE

router.get('/create', (req, res) => {
    res.send({categories: []}); // on renvoies des données qui seront nécessaire pour la company

});
// SAUVEGARDE OU L'INSERTION EN BDD

router.post('/', (req, res) => {


    // crée une noouvelle company / Vérifications / Autre méthode
    // const company =  new Company ({
    //     name: req.body.name,
    //     email: req.body.email,
    //   });

    const company = new Company(req.body);


    // sauvegarder la company
    company.save().then(() => {
        res.send({message: 'Company created successfully!'});

    }).catch(() => {
        res.status(500).send({message:'Error !'})
    })

    res.send({message: 'Company created successfully'}); // on envoie une réponse avec un msg
});


// R => READ => SHOW

router.get('/:id', (req, res) => {
    const id = req.params.id;
    Company.findById(id,(err, company) => {
        res.send(company);

    });
});


// U => UPDATDE

router.get('/edit/:id', (req, res) => {
    const id = req.params.id;
    Company.findById(id, (err, company) => {
        res.send(company);

    });
});

// SAUVEGARDER OU METTRE À JOUR LA COMPAGNIE

router.put('/:id',(req,res) => {

    if(!req.body.name){
        res.status(404).send({message: 'Name required'});
    }

    const id = req.params.id;
    Company.findByIdAndUpdate(id, req.body, {new: true}, (err, company)  => {


        if (!!err){
            res.status(404).send({message:'Company nod fund'});
        }
        res.send({message: 'Company update successfully !', company})
    });
});

// D => DELETE

router.delete('/:id', (req, res) => {

    res.send({message: 'Company deleted successfully'})
});

module.exports ={ // exports prefix et router 
    prefix,
     router
};
````


````js
//Fichier server.js 

const express = require ('express'); // on récupère express
const cors = require ('cors'); // on récupère cors
const mongoose = require('mongoose'); // on récupére mongoose

const CompaniesRouter = require ('./router/CompaniesRouter'); // {prefix, router}

// crée une connexion
mongoose.connect('mongodb://127.0.0.1/mean', {useNewUrlParser: true});

// test la connexion
const db = mongoose.connection;
db.on('error', (event) => {
    console.log('error:', event);
});
db.once('open',() => {
    console.log('Successfully connected');
});

const server = express(); // crée un serveur en exuctant la fonction express
const PORT = 8080; // crée une constant PORT

// midlware
server.use(express.json());

server.use(cors ({
    origin: '*',
    optionsSuccessStatus: 200,
}));
server.use(CompaniesRouter.prefix, CompaniesRouter.router);


server.listen(PORT, '127.0.0.1', function(){  // ecoute sur 8080 et sur 127.0.0.1 et on execute la fonction
    console.log('Server running on http://localhost:' + PORT);
});
````

- **Dans le dossier models** 
````js
// fichier Company.js

const mongoose = require ('mongoose') ; // on récupère mongoose

// Document
const CompanySchema = new mongoose.Schema({ // pour crée un schema on utilise mongoose
    // Configuration du document
    name: String,
    email: {
        type: String,
        default: 'ok@hotmail.com',
        required: true
    },
    description: String,
    date: Date,
});

// Crée un model
module.exports = mongoose.model('Company', CompanySchema);
````


___

⏯ - MongoDB - refactoring router => https://youtu.be/lHEaJUjh42w