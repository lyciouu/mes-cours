# AFFICHER LES INFORMATIONS D'UNE COMPAGNIE
___

⤵️ Exemple :

````js
// fichier server.js

// R => READ => SHOW

server.get('/companies/:id', (req, res) => {
    const id = req.params.id;
    Company.findById(id,(err, company) => {
        res.send(company);

    });
});
````

![img_1.png](../assets/img-mongodb/insomnia2.png)

___

⏯ - MongoDB - afficher les informations d'une compagnie => https://youtu.be/keFMuJCcInw