# La base de donnée 

- `map` : **garde en mémoire des objets** 

- `base de donnéé `: **c'est un entreport dans lequel on va enregistrer de la donnéé, ça sera une donnée tout le temps accessible** 

- `mySql` : **MySql est une base de donnée relationnelle qui comme son nom l'indique se requete à travers des query sql** 

`sql` :  **S**tructured **Q**uery **L**anguage 

◾ ️**Pour télécharger mamp sur macbook** :

1. Ce rendre sur le lien **https://www.mamp.info/en/downloads/**

2. Puis **télécharger l'application** 

3. Ce rendre sur **MAMP** et appuyer sur le **bouton à droite** **START** 

![img_1.png](../assets/img-live/start.png)

4. Cela va nous **rediriger** vers une **documentation** 

5. Puis aller dans **sql**, et cliquer sur le lien **phpMyAdmin**

![img_1.png](../assets/img-live/lien.png)

6.**Mysql** va fonctionner en **automie sur le serveur sql** 

![img_1.png](../assets/img-live/sql.png)

> On va pouvoir **piloter** et **administrer** de la **base de donnée** sur **mySql** portail pour **piloter la base de donner** 

◾️ **Pour crée une nouvelle base** :

1. Aller nouvelle base de données 

![img_1.png](../assets/img-live/new.png)

2. **Donner un nom a la base de donner**
3. Puis lui donner un encodage par défaut utiliser **utf8_general_ci** _**qui est util pour tout les types de cararctere**_ 
4. Puis cliquer sur crée 

`mysql est une base de donnée relationelle. `

◾️ **Crée une une feuille** :

1. **Aller sur SQL** 
   - `mysql impose de défénir le modele de donner avant de travailler avec`
    
2. Crée une table :  `CREATE TABLE` 

Pour définir une colone : **ouvrir les ()**

Pour définir une string en SQL : `varchar(255)`  **_et mettre par défaut 255 qui signfie le nb de cararctere_**

Pour définir un nombre en SQL : `int()`

Récuperer de la donner : `SELECT`

Inserer de la donner : `INSERT`

Crée de la donnée  : `CREATE`

Supprimer:  `DELETE` 

Mettre a jour : `UPDATE `

___

⏯ -  Point du 3 mars => https://youtu.be/Igi3CzkvonE

➡️ - https://www.w3schools.com/sql/sql_insert.asp

