# PREMIER ÉLÉMENT 
___

☞ Il faut retenir que React est juste une librairie qui nous permet de crée des composants, des composants qui vont gérer un état ce qui est donc très léger. Donc en plus de ça il faut une autre librairie React Dom qui va pouvoir attacher les composants au DOM. Car on peut développer des composants React et les éxécuté dans un navigateur.

On peut aussi les éxécuter sur un serveur avec ReactDomServer   

◾️ **Télécharger fichier de React** 

1. Aller sur la documentation React https://fr.reactjs.org/docs/cdn-links.html

2. Copier les Deux premier fichier du __CDN__
    - `ATTENTION POUR LA PRODUCTION IL VAUT MIEUX UTILISER LES FICHIER JS QUI SONT MINIFIER.`

3. Crée le template de base en écrivant : **!** puis appuyer sur **tab** 

4. Coller les fichier dans la balise body pour télécharger react 

⤵️ Exemple : **CODE**

````html
<!-- fichier index.html -->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> React Js </title>
</head>
<body>

<!-- fichier pour télécharger React et DOM -->
<script crossorigin src="https://unpkg.com/react@16/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
</body>
</html>
````

5. Cela doit nous afficher ceci (voir img) dans la console 

![img_1.png](../assets/img-react/reactdom.png)

◾️ **Crée son premier composant React** 

1. Dans le fichier html relier un fichier js exemple : **app.js** au fichier html exemple : **index.html** avec la balise script src

2. Appeler l'objet global **React** avec la méthode **createElement** dans le fichier app.js on lui donnera 3 arguments 

3. On l'enregistre dans une variable 

4. On affiche le contene de la varaible dans la console 

⤵️ Exemple : **CODE** 
````html
<!-- fichier index.html -->

<script crossorigin src="https://unpkg.com/react@16/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
<script src="app.js"></script> <!-- on ajoute relie le fichier js au fichier html -->
````

````js
const title = React.createElement('h1',{}, 'Hello World'); // React objet globale avec la méthode create élément et on lui donne 3 arguments 

console.log(title) // on affiche le contenue de la variable dans la console 
````

5. Cela va nous afficher dans la consolee (voir img) le h1, le tag qu'on a inséré en premiere arguement on va voir les propriété dont les enfants. **Les enfants c'est ce qu'on rentre à partir du 3eme argument** 

![img_1.png](../assets/img-react/composant.png)


◾️ **Pour afficher l'élément qui vient d'être crée dans le DOM** 

1. Utiliser  **ReactDOM**  avec la méthode **.render()** : **ReactDOM.render()** dans le fichier js 
   - render() car on veur rendre l'élément qu'on vient de crée 
   
2. Donc on lui donne la variable title  (1er argument)

3. En deuxieme argument on doit lui dire dans quel élément je veux inséré cet élément là( title ) donc pour inséré dans le body on utilise **document.body**

⤵️ Exemple : **CODE**
`````js
//  fichier app.js

const title = React.createElement('h1',{}, 'Hello World', 'Lycia');


ReactDOM.render( title, document.body ); // Utiliser ReactDOM.render() pour rendre l'élément qu'on veut crée et l'afficher dans le DOM.
//  ⚠️ document.body -> pour afficher dans le body MAIS DÉCONSEILLER ⚠️ 
console.log(title);
`````

4. Cela affichera 'Hello WorldLycia' dans le body (voir img) **avec une erreur dans la console** 

![img_1.png](../assets/img-react/render.png)

`ATTENTION: IL EST DÉCONSEILLER D'UTILIER document.body, CAR IL EST DÉCONSEILLER D'UTILISER LE BODY DIRECETEMENT ON VA DONC A LA PLACE CRÉE UNE DIV DANS LE FICHIER HTML `

⤵️ Exemple : 

````html
<!-- fichier index.html -->

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> React Js </title>
</head>
<body>

<div id="app"></div> <!-- on ajoute une DIV avec un ID  -->


<script crossorigin src="https://unpkg.com/react@16/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
<script src="app.js"></script>
</body>
</html>
````

````js
// fichier app.js 

const title = React.createElement('h1',{}, 'Hello World', 'Lycia');

ReactDOM.render( title, document.querySelector('#app') ) ; // ✅ utitliser le querySelector pour séléction un id ✅
console.log(title);

````

![img_1.png](../assets/img-react/div.png)

___Cette méthode est donc préférable___


◾️ **Avoir une méthode interactif** 

1. On va crée un compteur qu'on va nommée **Redirection dans** 
   
2. En deuxième enfant on va rajouter un deuxième élément on appelle donc **React** puis on crée un nouvel élément **React.createElement**

3. On mettra en premier argument une **span**, un objet vide **{}** qui correspondra au options de l'élément, puis on insére un enfant par exemple un nombre   

⤵️ Exemple : **CODE**

````js
// fichier app.js 

const title = React.createElement('h1',{},
        'Redirection dans :', // 
        React.createElement("span", {}, 1) // en deuxième enfant on rajoute un élément et on crée un nouvel élément 
);

ReactDOM.render( title, document.querySelector('#app') ) ;

console.log(title);
````
![img_1.png](../assets/img-react/redirection.png)

___Dans mon premier élément j'ai un enfant qui est la string 'Redirection dans :' et le deuxième élément et l'élemnt crée avec React : 1___

4. Pour que toute les 2 ont mette a jour ce nombre on enroule tout d'une fonction 


````js
// fichier app.js 

let n = 1 ; // on crée une variable qu'on va incrémenter 
function render () { // on enroule tout ça d'une fonction 
    // lorsqu'on appel render il va recrée un élément et va mettre a jour dans l'élément séléctioné #app
   const title = React.createElement('h1',{},
           'Redirection dans :',
           React.createElement("span", {}, n) // on insère la variable n 
   );

   ReactDOM.render( title, document.querySelector('#app') ) ;
}
render(); // on éxécute la fonction

setInterval(()=>{ // fonction fléché toute les seconde on re éxécute render 
   n++ // toute les seconde on incrémente de +1
   render()
},1000); // seconde 

````
![img_1.png](../assets/img-react/time.png)

___Toute les seconde cela va mettre a jour notre élément span___

`ATTENTION : IL EST IMPORTANT DE NOTER C'EST QUE CELÀ NE VA PAS REMETTRE À JOUR TOUT LE HTML IL REMET A JOUR SEULEMENT LA SPAN, IL NE REMET PAS A JOUR LE H1, LE PREMIER ÉLÉMENT QU'ON A CRÉE`


> __On préfèrera utiliser la syntaxe JSX est non la méthode JS qu'on a utilisé ci dessus  car c'est assez contraignant d'utiliser React.createElement ce qu'on va préférer faire c'est écrire en JSX car c'est plus simple et plus rapide.__ 

___

⏯ - React - Premier element => https://youtu.be/HCPBKUoZoHo

➡️ - https://fr.reactjs.org
