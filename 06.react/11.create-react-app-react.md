# CREATE REACT APP 

___

◾️ **Crée une application React via une commande** 

1. Ce rendre dans la documentation : https://fr.reactjs.org/docs/create-a-new-react-app.html
2. Copier la commande : npx create-react-app mon-app
3. Coller la commande sur le terminal 

☞ Celà va crée plusieurs dossiers
![img_1.png](../assets/img-react/create.png)

- `Le dossier public` : Contient tout les assets, logo, img ainsi que le fichier index.html qui sera l'entrée de notre application

- `Le dossier src` : Contient tout les composants, tout les éléments. Le dossier app.js est le seul élément
    - **Le dossier src est dans lequel on va développer notre application** 
    
- `Le dossier package.json` : On re va retourver les dépendances, il ya très peu de dépendances comparer à Angular. Il va aussi avoir les scripts qu'on pourra lancer. 
    - `start ` : Pour lancer le serveur locale
    - `build` : Pour lancer le scripts build 
    - `test` : Pour lancer le scripts test 
    - `eject` : Pour ejecter
    
◾️ Pour lancer le start de l'application 

1. Ouvrir le terminal 
2. Aller dans le dossier source en écrivant la commande cd suivi du dossier 
3. Puis écrire la commande npm run start 

☞ Cela va ouvrir un serveur locale et ouvrir un onglet 

![img_1.png](../assets/img-react/react.png)


◾️ Pour lancer le build du projet 

1. Ouvrir le termnial 
2. Lancer la commande cd suivi du nom de l'app
3. Puis lancer la commande npm run build 

☞ Celà crée plusieurs dossier

![img_1.png](../assets/img-react/build.png)

> Donc le **start** est pour lancer des **serveurs** de développement et si on veut mettre notre application en **production** il faut **build** le projet et ressortir des **assets mimifiés**, des **fichiers mimifiés**. Le fichier **index.html** sera **l'entrée** et sa sera aussi **mimifiés**, tout les **espaces** seront **supprimer** le code **sera sur une ligne**.   

☞ **Pour importer tout react** 

⤵️ Exemple :
````js
// fichier app.js
import * as React from 'react'; 
// importe tout react et on l'insère dans la variable react à partir de la dépendance react 
````

☞ **Pour importer avec la destructuration** 

⤵️ Exemple : 

````js
// fichier app.js
import {Component} from 'react'
// on séléctionne dans la librairie react Component 
````

**_On peut aussi lancer une application react en typescript_** 

___

⏯ - React - Create react app => https://youtu.be/FOk7T9D-5-o

➡️ - https://fr.reactjs.org/docs/create-a-new-react-app.html