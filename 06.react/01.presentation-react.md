# PRESENTATION REACT 

___

☞ **React JS**  est une **libraire Java Script** qui nous permet de crée des **S**ingle **P**age **A**pplication, donc des **applications interface utilisateurs** en Java Script. 

- **Date de création**: 2013 

React Js à été crée par l'**Équipe de Facebook** 

`ATTENTION : REACT JS N'EST PAS APPELER FRAMEWORK CAR C'EST UN ENSEMBLE DE FONCTIONNALITÉS QUI REND REACT TRÈS LEGER COMPARER ANGULAR QUI EST PLUS COMPLEXE   `
 

- **Objectif**  : Faciliter la création d'une interface utilisateur, chaque composant est liée à un état, et lorsque l'état est modifiée React va mettre à jour le html.

- **Boîtes qui utilise React**  : Facebook, Twitter, Airbnb, Instagram, Netflix etc..
    - React est utilisé presque dans toutes les grosses boîtes et celà montre le sérieux de la technologie 
    

___React est une petite librairie qui nous permet de crée des interfaces utilisateurs, mais comme cest une petite librairie on a la possibilité d'ajouter des librairie annexes qui vont nous permettre de développer notre application comme bon nous semble___

- **Avantage** : Facilité d'apprentissage 
- **Inconvénients de React** : Beaucoup trop de choix, libre de faire bcp trop de choses 
    - Pour crée de grosses application on va devoir séléctionner d'autres librairie il y aura donc beaucoup trop de choix au niveaux de la conception d'une application 

**React** contrairement à Angular a une **syntaxe totalement différente**.

- **Syntaxe de développement** :  En React on développe en JSX, c'est une **SURCOUCHE** du Java Script  

__Avec React on peut développer des applications en natif, avec la librairie react natif on peut développer en IOS pour Iphone et des applications en Kotlin pour android__

Un composant gère un état et lorsque l'état est mis à jour le composant rect va mettre à jour le composant et le html. **Chaque composant gère leurs état** 

___

⏯ - React - Presentation => https://youtu.be/Bd06YvcdbmI

➡️ - https://fr.reactjs.org
