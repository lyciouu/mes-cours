# CRÉER UNE TABLE, DÉCOUVERTE D'OPTIONS 
___

◾️ **Crée une table** 

1. Faire **clique droit** sur **tables** 
2. Puis cliquer sur **create tables** 
3. Donner un **nom** à la **table**

![img_1.png](../assets/img-mysql/colone.png)
  
- `1ère colonne` : **nom de la colone**
- `2ème colonne `: **choisir le type de la colonne** 
   - `INT` : **Entier nombre entier** 
   - `VARCHAR` : **Chaine de caractère** 
   - `DECIMALE` : **Décimale**
   - `DATETIME `: **Date et une heure des minutes et seconde** 
   - `BIGINT()` : **Nombre très grand** 
   - `TINYINT()` : **Nombre contenant que 0 ou 1**
   - `LONGTEXT` : S**tocker de long texte**
  
![img_1.png](../assets/img-mysql/colone2.png)

- `PK` : **Primary Key permet d'identifier chaque enregistrement dans une table**
- `NN` : **Not Null permet que cette colonne ne peut pas contenir de valeur null**
-` UQ` : **Unique permet d'indiquer que cette colonne soit unique** 
- `BIN` : **Permet d'indiquer que nous allons contenir dans cette colonne une donnée de type binaire et non pas texte**  
-` UN` : **Permet que cette colonne va contenir un nombre non signé c'est à dire un nombre qui n'est pas signé par positif ou négatif** 
- `ZF` : **Zéro file permet de remplir les espaces** 
- `AI` : **Permet que cette colonne va s'auto incrémenter** 
- `G` : **Generated permet d'indiquer que cette colonne va contenir une valeur qui va etre calculer. Cette valeur peut avoir deux type soit virtuel soit stocker** 
- `DEFAULT `: **Permet d'inséré une valeur par défaut** 


    
◾️ **Configuration basique** 

![img_1.png](../assets/img-mysql/config.png)


___

⏯ - MySQL - créer une table découverte des options => https://youtu.be/IVE7pGjknSc




