# COMPTER LES DONNÉES AVEC LA FONCTION COUNT

___

⤵️ Exemple : **Compter tout les éléments d'une table avec ( * )** 

`Il faut utiliser la fonction COUNT qu'on entoure autour de l'étoile`

![img_1.png](../assets/img-mysql/count.png)

`Celà va crée une colonne nommé COUNT(*) et le nombre à l'interieur va être le nombre d'élément que j'ai`

![img_1.png](../assets/img-mysql/count2.png)

⤵️ Exemple : **Compter tout les éléments d'une table avec (id)** 

` Compter le nom via le nom d'une colonne id`

![img_1.png](../assets/img-mysql/count3.png)

![img_1.png](../assets/img-mysql/count4.png)

⤵️ Exemple : **Compter et renommer une colonne** 

` Renommer le nom de la colonne avec le mot clé as suivi du nouveau nom`

![img_1.png](../assets/img-mysql/count5.png)

`C'est total qui est afficher et plus COUNT`

![img_1.png](../assets/img-mysql/count6.png)

___

⏯ - MySQL - compter les données avec la fonction count => https://youtu.be/_x-5nrTnQww