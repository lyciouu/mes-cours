# INSTALLATION
___

◾️ **Pour télécharger MySQL**

1. Se rendre sur le site : **https://dev.mysql.com/**
2. Dans la barre de navigation cliquer sur:  **DOWNLOADS** 
3. Puis cliquer en bas sur le lien : **MySQL Community (GPL) Downloads** 
4. Cliquer sur le lien : **MySQL Community Server**
5. Cliquer sur la premier lien pour télécharger 
6. Cliquer sur le lien : **No thanks, just start my download**
7. Puis télécharger le logiciel
8. Faire l'installation en appuyant tout le temps sur contiuner 

◾️ **Intaller un logiciel pour manipuler les base de données : WORKBENCH**

1. Se rendre sur ce lien : **https://dev.mysql.com/downloads/workbench/**
2. Cliquer sur le premier fichier à télécharger 
3. Cliquer sur le lien : **No thanks, just start my download**
4. Installer le logiciel 


![img_1.png](../assets/img-mysql/interface.png)

___

⏯ - MySQL - installation => https://youtu.be/waf2ljjGvjg

