# ORM
___

☞ **Sequelize** est ce qu'on appelle un **ORM**. 

- `ORM` : **O**bject-**R**elational **M**apping,
    - C'est un **ensemble de classe** permettant de **manipuler les tables** de **base de données relationnel** comme si il s'agissait **d'objet**. Un **ORM** est une **interface d'accès** à la **base de données** qui donne **l'illusion** de ne plus travailler avec des **requêtes SQL** mais de **manipuler des objets**. L'**avantage** de **cette couche d'abstraction** est qu'il n y a plus besoin de ce **soucier du système** de **base de données** utilisé, c'est l'**ORM** qui est à la **charge** de **transfomer les requêtes** pour la **rendre comptabile** avec la **base de données**. L'**ORM** va s'**appuyer** sur **des modèles** et un **modèle représente** une **table** de la **base de données**.     

![img_1.png](../assets/img-sequelize/orm.png)

___

⏯ - Sequelize - orm => https://youtu.be/5ZHTB3k5lbA