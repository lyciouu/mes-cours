# INSTALLATION 
___


◾️ **Installer Sequelize** 

1. Ce rendre sur le site http://sequelize.org
2. Puis cliquer sur **v6 Documentation** pour **accèder à la documention version 6 de Sequelize** 
3. Puis aller sur : **Getting Started** et cliquer sur **Installing**
4. Copier la commande **npm install --save sequelize**
5. **Coller la commande** dans le terminal 
6. Puis **intaller le driver** **MySQL** en copiant la commande **npm install --save mysql2**
7. Cela va installer l'**ORM sequelize** et le **driver MySQL**

![img_1.png](../assets/img-sequelize/sequelize2.png)

![img_1.png](../assets/img-sequelize/mysql2.png)

___

⏯ - Sequelize - installation => https://youtu.be/w6_vEJ3JYV4