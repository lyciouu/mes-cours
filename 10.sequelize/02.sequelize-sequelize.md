# SEQUELIZE 
___

☞ **Sequelize** est un **ORM NodeJS** basé sur les ****p**romesse**. Il est **compatible avec MySQL** et d'**autres système de gestion de base** **de données**. On va pourvoir **connecter** notre **API** à notre **base de données MySQ**L pour : 
- **sotcker** une liste d'utilisateurs 
- **ajouter** un utilisateur
- **modifier**  un utilisateur 
- **supprimer** un utilisateur 
- et **récupéré des données**. 

L'idée est de **simplifier** le **développement**. Nous n'aurons plus à écrire **les requêtes SQL** qui peuvent être **longue** et **répétitive**. À la place nous utiliserons : 
- Les **classes** 
- Les **méthodes** 

fourni par **Sequelize** pour réaliser ses **requêtes**.

![img_1.png](../assets/img-sequelize/sequelize.png)

---

⏯ - Sequelize - sequelize => https://youtu.be/3UujbFFSUX0