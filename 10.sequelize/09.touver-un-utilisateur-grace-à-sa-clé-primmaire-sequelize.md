# TROUVER UN UTILISATEURS GRÂCE À SA CLÉ PRIMMAIRE
___

⤵️ Exemple : 

````js
// fichier server.js

server.get('/users/:id', function (request, response){

    User.findByPk(request.params.id)
        .then((user) => {

            if(!!user) {
                response.send(user);
            } else {
                response.statusCode = 404;
                response.send({message: 'Aucun utilisateur trouvé'});
            }
        })
        .catch((err) => {
            response.statusCode = 404
            response.send('Une erreur est survenue : ' + err)
        });

});

````

1. Puis écrire la commande node **server.js**
2. Et se rendre sur **Insomina** dans **Get info** 

` on voit qu'ont a bien récupéré l'id 2 `

![img_1.png](../assets/img-sequelize/insomnia4.png)

![img_1.png](../assets/img-sequelize/mysql4.png)

___

⏯ - Sequelize - trouver un utilisateur grace à sa clé primaire => https://youtu.be/LqYS2sPjJHM