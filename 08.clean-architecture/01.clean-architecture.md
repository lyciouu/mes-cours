# CLEAN ARCHITECTURE 
___

☞ La **clean architecture** c'est un **process**, qui va nous permettre de **coder** avec **méthode** et **rigueur**. Et surtout **vraiment comprendre** le **travail qu'on va réaliser**.

**Il faut d'abord faut voir de quoi parle le projet, car la clean architecture est un rassemblement de process qui nous permettent de faire en sorte que notre code soit lisible  par des développeur ou chef de projet ou personne qui ne comprennnt rien au code** 


⤵️ Exemple : 

1. Regarder le **User stories** et **modéliser** les choses 

`En tant qu'utilisateur, je veux pouvoir m'enregistrer dans le système afin de pouvoir me retrouver via mon nom. `

☞ Cela veut dire **je suis un utilisateur** et je souhaite pouvoir **m'enregistrer** dans le système afin de **pouvoir me retrouver** via mon nom

**_On as donc  deux choses à faire, une action d'enregistrement et une action de lecture_** 


2. Dans la console faire un **npm init** pour initier le projet 
3. Écrire la commande **yarn add -D typescript** afin d'installer typeScript 
4. Écrire la commande **yarn add -D tsc** afin d'installer tsc 
5. Puis écrire tsc init 
6. Crée un dossier src
7. D Créee un dossier domain 
8. Dans le dossier domain crée un dossier entities 
9.

  PAS FINI 
