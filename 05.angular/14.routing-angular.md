# ROUTING 
___


☞ Angular est un framework de **S**ingle **P**age **A**pplication, ce qui va être généré par __Angular__ c'est __une seule page__ qui va être chargée et ensuite on va éxécuté tout le code JS. Donc par définiton ça va être compliqué de développer plusieurs page HTML, c'est donc Angular qui va se charger grâce au système de routeur de simmuler plusieur page HTML. Donc en fonction de l'url je veux indiquer à Angular que je veux afficher un certains composant. Donc à chaque fois que l'URL va changer Angular va se charger d'écouter les modifications de l'URL et d'afficher les composant qui corresponde, c'est donc ce qu'on appel le système de __routing__.



⤵️ - Exemple : Configurer un routing 

Dans le fichier app.modules.ts il faut importer dans les dépendence  les modules "__RouterModule__" pour pouvoir bénéficier des fonctionnalités de ce modules. Il faut après appeler la méthode __.forRoot()__ et on passera en argument des routes. 


`````ts


import {RouterModule, Routes} from "@angular/router";

const routes : Routes = [ // déclare la constante qu'on type avec 'Routes'
    {path: 'contact', component: ContactComponent}, // composant contact 
    {path: 'users/info', component: InfoComponent }, // composant info
    {path: 'users', component: UsersComponent}, // composant users
    {path: '', component: Dashboard},// tout les composant sont déclarer ici 
]

@NgModules ({
    déclarations: [
        AppComponent,
        DashboardComponent,
        ContactComponent,
        UsersComponent,
        InfoComponent,
    ],

    imports:[
        RouterModule.forRoot(routes) // on importe le module,  on appel la méthode puis on passe en arguent 'routes' 
    ]
})

`````

___Il faut ensuite afficher les components dans le template.___ 

````html
<!-- Dans le fichier app.modules.html -->

<router-outlet></router-outlet> 
````

☞ Pour crée un lien vers la page souhaitée on va utiliser le __routerLink__

⤵️ Exemple : 

`````html
<!-- navbar.component.html -->

<li class="nav-item"> 
  <a class="nav-link" routerLink="/">Dashboard</a> // on veut aller sur l'url dashbord 
</li>

<li class="nav-item"> 
  <a class="nav-link" routerLink="/users">Users</a> // url users
</li>

<li class="nav-item">
    <a class="nav-link" routerLink="/users/info">Info</a> // url info
</li>

<li class="nav-item">
    <a class="nav-link" routerLink="/contact">Contact</a> // url contact 
</li>

`````

⤵️ Exemple : Classe Active 

`````html
<!-- navbar.component.html -->

<li class="nav-item" routerLink="/" routerLinkActive="active" [routerLinkActiveOptions]="{exact: true"> // lien actif que si la condition est true 

    <a class="nav-link" >Dashboard</a>
</li>

<li class="nav-item" routerLink="/users" routerLinkActive="active">
    <a class="nav-link" >Users</a>
</li>

<li class="nav-item" routerLink="/users/info" routerLinkActive="active">
    <a class="nav-link" >Info</a>

<li class="nav-item" routerLink="/contact" routerLinkActive="active">
    <a class="nav-link"  >Contact</a> // url contact
</li>    


`````

___

⏯ - Routing  : https://youtu.be/4G_Uzu1KVGA