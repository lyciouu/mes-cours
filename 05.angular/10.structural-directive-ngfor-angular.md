# STRUCTURAL DIRECTIVE : NGFOR

___

☞ Structural Directive ngFor, nous permet d'itérer dans le DOM.

Syntaxe : __Dans la baslise *ngFor="let user (variable) of users ( valeur du tableau)"__ 

⤵️ Exemple : 

````ts
// fichier app.component.ts

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'Structural Directive : ngFor';
 show: boolean = false;
 
 users: {name: string, color?: string}[] = [ 
   {name: 'Lycia', color: 'pink'},
   {name: 'Merouane'},
   {name: 'Ilyes', color: 'blue'}
 ]; // Tableau d'objet

}

````

```html
<!--fichier app.component.html -->

<h1> {{ title }}</h1>


<!-- on veut afficher tout les utilisateurs  -->

<p *ngFor="let user of users"> 
  {{ user.name}} <!-- accessible seulement à l'intérieur -->
</p> <!-- cela va duppliquer 3x l'élément p -->

<button (click)="show =!show"> Toggle title </button>


```

↕️↕️↕️↕️↕️↕️

![img_1.png](../assets/img-angular/ngfor.png)

⤵️ Exemple : Accèder à l'index 

Syntaxe pour accèder à l'index  : __*ngFor="let user of users; let id (création d'une variable) = index (position)" puis on interpole l'id {{ id }}__

````ts
// fichier app.component.ts

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'Structural Directive : ngFor';
 show: boolean = false;
 
 users: {name: string, color?: string}[] = [ 
   {name: 'Lycia', color: 'pink'},
   {name: 'Merouane'},
   {name: 'Ilyes', color: 'blue'}
 ]; // Tableau d'objet

}

````

```html
<!--fichier app.component.html -->

<h1> {{ title }}</h1>


<!-- on veut afficher tout les utilisateurs  -->

<p *ngFor="let user of users; let id = index">  <!-- pour chaque valeur du tableau on aura accès a la valeur qu'on va inséré dans la varaible user et on va avoir accès a l'index    -->
 {{ id }} - {{ user.name}} <!-- accessible seulement à l'intérieur -->
</p> <!-- cela va duppliquer 3x l'élément p -->

<button (click)="show =!show"> Toggle title </button>


```

↕️↕️↕️↕️↕️↕️

![img_1.png](../assets/img-angular/ngfor-index.png)


◾️ Il est possible de récupérer d'autre informations telles que l'index de l'élément :

- `index` : position de l'élément.
- `odd` : indique si l'élément est à une position impaire.
- `even` : indique si l'élément est à une position paire.
- `first` : indique si l'élément est à la première position.
- `last` : indique si l'élément est à la dernière position.


⤵️ Exemple : Imutabilité 

`````ts
// fichier app.component.ts

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title: string = 'Structural Directive : ngFor';
    show: boolean = false;

    users: { name: string, color?: string }[] = [
        {name: 'Lycia', color: 'pink'},
        {name: 'Merouane'},
        {name: 'Ilyes', color: 'blue'}
    ]; // Tableau d'objet

    deleteUser(id): void { // fonction qui renvoie rien, on récupere user et id
        // grace a l'id on va pouvoir supprimer l'utilisateur du tableau
        let arr = [...this.users]; // crée un nouveau tableau auquel on va mettre toutes les valeur du tableau users
        arr.splice(id, 1) // on utilise la methode splice pour supprimer la postion qui se trouve a la position id et on supprime un element
        this.users = [...arr]; // on remet a jour le tableau users avec un new tableau  

        console.log(id)
    }

}

`````

`````html
<!--fichier app.component.html -->

<h1 *ngif="show"> {{ title }}</h1>


<!-- on veut afficher tout les utilisateurs  -->

<div *ngFor="let user of users; let id = index "> <!-- pour chaque valeur du tableau on aura accès a la valeur qu'on va inséré dans la varaible user et on va avoir accès a l'index    -->
  <div>{{ id }} - {{ user.name}} </div> <!-- accessible seulement à l'intérieur  -->

  <button (click)="deleteUser( id)">afficher</button>
<!-- on ajoute un bouton, qunand je clique sur le bouton affichier on veut afficher la fonction deleteUser   -->
</div>

<button (click)="show =!show"> Toggle title </button>


`````

↕️↕️↕️↕️↕️↕️

![img_1.png](../assets/img-angular/immutabilité.png)

___Si je clique sur afficher ça va supprimer l'élément___

⤵️ Exemple : __Mettre ngIf et ngFor__

`````ts
// fichier app.component.ts

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'Structural Directive : ngFor';
 show: boolean = false;

 users: {name: string, color?: string}[] = [
   {name: 'Lycia', color: 'pink'},
   {name: 'Merouane'},
   {name: 'Ilyes', color: 'blue'}
 ]; // Tableau d'objet

  deleteUser( id): void{ // fonction qui renvoie rien, on récupere user et id
    // grace a l'id on va pouvoir supprimer l'utilisateur du tableau
    let arr = [...this.users]; // crée un nouveau tableau auquel on va mettre toutes les valeur du tableau users
    arr.splice(id,1) // on utilise la methode splice pour supprimer la postion qui se trouve a la position id et on supprime un element
    this.users = [...arr]; // on remet a jour le tableau users avec un new tableau

    console.log(id)
  }

}

`````

````html
<!--fichier app.component.html -->

<h1 *ngIf="show"> {{ title }}</h1>

<!-- ng container n'est pas un élément html -->
<ng-container *ngFor="let user of users; let id = index "> <!-- permet d'encapsuler du html et le rendre comme tel -->

  <div *ngIf="user.color"> <!-- on veut afficher tout les utilisateur si ya la prorpiété color -->

    <div>{{ id }} - {{ user.name}} | {{user.color}} </div> <!-- accessible seulement à l'intérieur  -->

    <button (click)="deleteUser( id)">afficher</button>

  </div>

</ng-container>

  <button (click)="show =!show"> Toggle title </button>



````

↕️↕️↕️↕️↕️↕️

![img_1.png](../assets/img-angular/ngif-ngfor.png)

___

⏯ - Angular - Structural directive ngfor : https://youtu.be/E2KwZMS6KyI

➡️ - https://guide-angular.wishtack.io/angular/composants/ngfor