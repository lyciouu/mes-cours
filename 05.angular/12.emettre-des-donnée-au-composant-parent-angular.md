# ÉMETTRE DES DONNÉES AU COMPOSANT PARENT 
___


☞ __@Output()__ marque une propriété dans un composant enfant comme une porte par laquelle les données peuvent passer de l'enfant au parent.

⤵️ Exemple : 

````ts
import {Component, Output, EventEmitter} from '@angular/core';
//Rajouter l'input et l'event emitter

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent {

@Output('task') taskEmit = new EventEmitter(); // décorateur output pour émettre des sortie
    // a comme valeur une nouvelle instance evenEmitter, on émet un flux de donées

addTask(): void{

}
}

````

```html
<app-add-task (task)="addTask($event)"></app-add-task> // ecoute d'évenement 
```

___

⏯ - Angular - Émettre des données au composant parent : https://youtu.be/tzPiI8_dAoc

➡️ - https://angular.io/guide/inputs-outputs