# ROUTING : ROUTER SERVICE 
___

☞ Ici on va changer l'url de manière programmatique 

⤵️ Exemple : Pour changer l'url 

`````ts
// fichier users.component.ts

import {Router} from "@angular/router"; // importation 

export class UsersComponent implements Oninit {

 constructor(private router: Router) { // on injecte le router qu'on type avec Router
 
}


  ngOnInit(){
   setTimeout(() =>{
//Au bout de 3secones redirige vers les infos de User1   
  this.router.navigateByUrl('/users/1');

}, 3000);
}
public showInfo(number){ // fonction 
 setTimeout();
this.router.navigateByUrl('/users' + id) ;// on appel router et on va pouvoir naviguer grace a la méthode
}

}

`````

````html
<ul>
  <li *ngFor="let user of users" 
      (click)="showInfo(user.id)"> <!-- écoute événement du click -->
{{ user.id }}, {{ user.name }} 
  </li>
</ul>

````

___

⏯ -  Routing : Router service  : https://youtu.be/3Hna49OPdEU