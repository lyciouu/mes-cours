# SERVICE ET INJECTIONS DES DÉPENDANCES 

___

☞ __Un service__ c'est une __classe__ qui peut être **réutilisée** dans **plusieurs composant**, on utlise les services pour **plusieurs objectif** pour faire des **appels http**, **requêter des données** à un serveur, pour **ressortir un code** et le rendre réutilisable à plusieurs composant, et on peut utiliser des services pour **manager des données**.

◾️ **Pour crée un service** 

1. Ouvrir le terminal 
2. Écrire dans le terminal **ng g service users**, ou **ng generate service users**  avec le nom du service 
3. Celà va generer un fichier de test et un fichier qui contient le service

◾️ **Pour garder le code réutilisable et bien sectionner** 

1. Dans le dossier app, crée un dossier service
2. Dans le dossier service c'est un dossier users
3. Copier les 2 fichier crée dans le dossiers users 


☞ L'injection est système généré par __Angular__ qui permet dans les composant d'injecter des services pour pouvoir le réutiliser à plusieurs endroit  

⤵️ Exemple :

````ts
// fichiers users.service.ts

import {Injectable} from 'angular/core' ;

@Injectable({ // décorateur 
    providedIn: 'root' // fourni a la racine 
})
export class UsersService { // class exportable qui se nomme UsersService  et décorer par le décorateur Injectable fourni a la racine 
users = ['Lycia'] ; // accessible sur plusoerus composant 
    constructor() {}
    getUsers() { // fonction 
        return this.users; // retourne tout les utilisateur 
    }
}
````

````ts
// fichier users.components.ts 

constructor(private usersService: UsersService) {  // angular va pouvoir injecter dans dans cette variable une insta,ce de usersService 
    
}

ngOnInit() {
    const users=  this.usersService.getUsers(); // appel la variable et on appel getUsers
}

````
☞ Injection de dépendance c'est justement __Angular__ qui va se charger de crée une instance qui va être accessible partout dzns notre modules


