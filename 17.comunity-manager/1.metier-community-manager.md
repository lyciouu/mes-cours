# COMMUNITY MANAGEMENT

## Sommaire

1. [Appelation metiers](#appellation-metiers)
2. [Les competences](#les-competences)
3. [Les missions](#les-missions)
___

## Appellation metiers

> Personne qui s'occupe de la communication digtale et la stratégie digitale est appeler un **community manager**

❗️Attention à ne pas confondre le métier de community manager à celui de social média manager❗️ 

`Le social media manager s'occupe du digital alors que le community manager la met en application` 

___

## Les competences

- Qualités humaine **INDISPENSABLE** afin de dévlopper une véritable relation qualitative avec vos communauté. 
- Qualité rédactionnel **FACULTATIF**
- Culture générale **NECESSAIRE**
- Qualité technique **INDISPENSABLE** (adobe, html indesign etc..)
  
 ___

## Les missions

 ![img.png](../assets/community-manager/community.png)

**Différentes stratégie de community manager**
- stratégie de communication de crise
- stratégie digitale
- stratégie pull and push 
- stratégie d'influence 
- stratégie de relation public
- startégie éditorial
  
 ___
  
  ## La pyramide de maslow

  `L'objectif finale du community manager et d'interpeller l'internaute pour qu'il devienne client`


![img.png](../assets/community-manager/pyramide.png)
____
## Le schema de l'acte d'achat

![img.png](../assets/community-manager/achat.png)

> Ce schéma résume la relation actuelle entre le client et les entreprises. Le client est un internaute consommateurs, acteurs, décisionnaire. Le community manager intervient à chaque étape d'achat, et s'assurer que chaque internaute consommateurs acteurs décisionnaire soit satisfait. 
___

## Pull & push 

- Mode **push** -> **Outbound marketing** -> Pousser le produit vers le consommateur afin de l'inciter à l'achat 
- Mode **pull** -> **Inbound marketing** -> Faire venir le futur client à soit en l'amenant en douceur afin de devenir un vrai client en lui apportant du contenue utile et une forte valeur ajouté. 
  
___

## Histoire d'internet

On distingue 3 générations d'internet :

   1. 1970 création annuaire du web, yahoo aol. Moteur de reherches (google) wikipédia youtube. Naissance des média sociaux 
2. 2000-2015 migration de l'ordinateur au téléphone portable avec les application mobile 
2. 2015 internet des objets (robot, montre, jouet)
   
 ___
 
 ## Media sociaux 

 `Média sociaux`-> Désigne généralement l'ensemble des sites et plateforme web qui propose des fonctionnalitées dites sociale au utilisateurs. Création collaborative des contenus, échange d'information entre individu et partage de contenu. Les médias sociaux ont été crées pour encourager la participation des internautes pour contribuer et donner leurs avis supprimant ainsi la barriere entre le public et les médias.  

 Différentes catégorie de médias sociaux 

 - Blog ( journal intime, écriture d'articles ) 
 - Forum ( forum de conversation avec le meme sujet de disscussion) ex: au feminin
 - communauté de partage en ligne (photos vidéos etc )ex: fb insta flickr 
 - agrégateur d'actualité (pour l'actualité ) ex : scoopit 
 - espace de collaboration (wiki)
  
___





